/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/23 12:17:08 by jwong             #+#    #+#             */
/*   Updated: 2018/05/02 11:37:53 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include "libft.h"
#include "fractol.h"

void	error_system(void)
{
	char	*err;

	err = strerror(errno);
	write(0, err, ft_strlen(err));
	write(0, "\n", 1);
	exit(-1);
}

void	error_program(void)
{
	char	*str;

	str = "Types of fractals\nmandelbrot\njulia\nburning_ship\nnova\n\n"
		"usage: ./fractol [fractal type]\n";
	write(0, str, ft_strlen(str));
	exit (-1);
}

int	error_handler(int err)
{
	if (err == ARG_ERR)
		error_program();
	else if (err == MALLOC_ERR)
		error_system();
	return (-1);
}
