/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mandelbrot.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/01 18:16:58 by jwong             #+#    #+#             */
/*   Updated: 2018/05/01 18:16:59 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include "fractol.h"

static int		mandelbrot_calc(double *z_re, double *z_im, int iter)
{
	double	z_re2;
	double	z_im2;
	double	old_z_re;
	double	old_z_im;
	int		i;

	old_z_im = *z_im;
	old_z_re = *z_re;
	i = 0;
	while (i < iter)
	{
		z_re2 = *z_re * *z_re;
		z_im2 = *z_im * *z_im;
		if (z_re2 + z_im2 > 4)
			break ;
		*z_im = 2 * *z_re * *z_im + old_z_im;
		*z_re = z_re2 - z_im2 + old_z_re;
		i++;
	}
	return (i);
}

void	mandelbrot(t_win *win, int x, int y)
{
	double	z_re;
	double	z_im;
	int		i;

	z_im = px_to_mand_y(win, y);
	z_re = px_to_mand_x(win, x);
	i = mandelbrot_calc(&z_re, &z_im, win->frac->max_iter);
	if (win->colour_mode == FALSE)
		put_pixel(win, x, y,
			colour_smoothing(win, sqrt(z_re * z_re + z_im * z_im), i));
	else
		put_pixel(win, x, y, multi_colour(win, i));
}

/*
static int		mandelbrot_calc(double *ptr_z_re, double *ptr_z_im, int iter)
{
	register double  z_re;
	register double  z_im;
	register double	z_re2;
	register double	z_im2;
	register double	init_z_re;
	register double	init_z_im;
	int		i;

    z_re = *ptr_z_re;
    z_im = *ptr_z_im;
	init_z_im = z_im;
	init_z_re = z_re;
	i = 0;
	while (i < iter)
	{
		z_re2 = z_re * z_re;
		z_im2 = z_im * z_im;
		if (z_re2 + z_im2 > 4)
			break ;
		z_im = 2 * z_re * z_im + init_z_im;
		z_re = z_re2 - z_im2 + init_z_re;
		i++;
	}
	*ptr_z_re = z_re;
	*ptr_z_im = z_im;
	return (i);
}

void	mandelbrot(t_win *win, int x, int y)
{
	double	z_re;
	double	z_im;
	int		i;

	z_im = px_to_mand_y(win, y);
	z_re = px_to_mand_x(win, x);
	i = mandelbrot_calc(&z_re, &z_im, win->frac->max_iter);
	if (win->colour_mode == FALSE)
		put_pixel(win, x, y,
			colour_smoothing(win, sqrt(z_re * z_re + z_im * z_im), i));
	else
		put_pixel(win, x, y, multi_colour(win, i));
}
*/
