#include <stdlib.h>
#include "mlx.h"
#include "libft.h"
#include "fractol.h"

t_frac  *init_frac(void)
{
    t_frac  *new;

	new = (t_frac *)malloc(sizeof(t_frac));
	if (!new)
		error_handler(MALLOC_ERR);
	ft_bzero(new, sizeof(new));
	new->max_re = MAX_RE;
	new->min_re = MIN_RE;
	new->max_im = MAX_IM;
	new->min_im = MIN_IM;
    new->max_iter = MAX_ITER;
	return (new);
}

void	init_win(t_win *win)
{
	win->mlx = mlx_init();
	win->win = mlx_new_window(win->mlx, MAX_WIN_X, MAX_WIN_Y, "fractol");
	win->img_id = mlx_new_image(win->mlx, MAX_WIN_X, MAX_WIN_Y);
	win->img = mlx_get_data_addr(win->img_id, &win->bpp, &win->size_line, &win->endian);
    win->lock = FALSE;
	win->colour_mode = FALSE;
    win->freq = FREQUENCY;
    win->zoom = ZOOM;
}

int     set_fractal(t_win *win, char *param)
{
    char    *str;

	str = (char *)malloc(sizeof(char) * (ft_strlen(param) + 1));
	if (!str)
		error_handler(MALLOC_ERR);
    string_to_lower(param, str, ft_strlen(param));
    win->frac = init_frac();
    if (ft_strcmp("mandelbrot", str) == 0)
        win->fractol = MANDELBROT;
    else if (ft_strcmp("julia", str) == 0)
    {
        win->fractol = JULIA;
        win->frac->c_re = -0.75;
        win->frac->c_im = -0.11;
    }
    else if (ft_strcmp("burning_ship", str) == 0)
        win->fractol = BURNING_SHIP;
    else if (ft_strcmp("nova", str) == 0)
        win->fractol = NOVA;
	else
	{
		free(str);
		return (FALSE);
	}
	free(str);
    return (TRUE);
}
