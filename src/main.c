/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/01 18:16:49 by jwong             #+#    #+#             */
/*   Updated: 2018/05/01 18:16:50 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mlx.h"
#include "libft.h"
#include "fractol.h"

int		main(int argc, char **argv)
{
	t_win	win;

	if (argc != 2)
		error_handler(ARG_ERR);
	ft_bzero(&win, sizeof(win));
	if (set_fractal(&win, argv[1]) == FALSE)
		error_handler(ARG_ERR);
	init_win(&win);
    draw_fractal(&win);
	mlx_key_hook(win.win, key_handler, &win);
    mlx_mouse_hook(win.win, mouse_click_handler, &win);
    mlx_hook(win.win, 6, (1L << 6), mouse_over_handler, &win);
	mlx_put_image_to_window(win.mlx, win.win, win.img_id, 0, 0);
    mlx_loop_hook(win.mlx, redraw, &win);
	mlx_loop(win.mlx);
	return (0);
}
