/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   util.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/11 14:31:06 by jwong             #+#    #+#             */
/*   Updated: 2018/04/26 15:55:14 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stdlib.h"
#include "mlx.h"
#include "libft.h"
#include "fractol.h"

char	*string_to_lower(const char *src, char *dest, int len)
{
	int	i;

	i = 0;
	while (src && i < len)
	{
		dest[i] = ft_tolower(*src);
		i++;
		src++;
	}
	dest[i] = '\0';
	return (dest);
}

void	put_pixel(t_win *win, int x, int y, int rgb)
{
	int size;

	if (x >= MAX_WIN_X || x < 0 || y >= MAX_WIN_Y || y < 0)
		return ;
	size = (y * win->size_line) + (x * win->bpp / 8);
	if (win->endian == 0)
	{
		win->img[size] = mlx_get_color_value(win->mlx, rgb);
		win->img[size + 1] = mlx_get_color_value(win->mlx, (rgb >> 8));
		win->img[size + 2] = mlx_get_color_value(win->mlx, (rgb >> 16));
	}
	else
	{
		win->img[size] = mlx_get_color_value(win->mlx, (rgb >> 16));
		win->img[size + 1] = mlx_get_color_value(win->mlx, (rgb >> 8));
		win->img[size + 2] = mlx_get_color_value(win->mlx, rgb);
	}
}

int     iterate_coordinates(t_win *win, void (*fractal)(t_win *, int, int))
{
    int     x;
    int     y;
    
    win->frac->im_factor = scale_factor_im(win->frac);
    win->frac->re_factor = scale_factor_re(win->frac);
    y = 0;
    while (y < MAX_WIN_Y)
    {
        x = 0;
        while (x < MAX_WIN_X)
        {
            fractal(win, x, y);
            x++;
        }
        y++;
    }
    return (0);
}