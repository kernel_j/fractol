/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   zoom.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/01 18:17:09 by jwong             #+#    #+#             */
/*   Updated: 2018/05/01 18:17:10 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
#include <stdio.h>

/*
** modify min and max so that the cursor stays in the same point:
** (new_max - c) / (new_min - c) = (old_max - c) / (old_min - c)
** (old_max - old_min) * zoom = new_max - new_min
** where c is the mouse cursor position
*/
void    zoom_axis(double *min, double *max, double axis, double zoom)
{
    double  old_min;
    double  old_max;
    double  tmp;
    double  de;
    double  nu;

    old_min = *min;
    old_max = *max;
    tmp = ((old_max * axis) - (axis * axis)) / (old_min - axis);
    nu = (((old_max - old_min) * zoom) - axis) + tmp;
    de = ((old_max - axis) / (old_min - axis)) - 1;
    *min = nu / de;
    *max = ((old_max - old_min) * zoom) + *min;
}

void    zoom_in(t_win *win, double x, double y)
{
    zoom_axis(&win->frac->min_re, &win->frac->max_re, x, win->zoom);
    zoom_axis(&win->frac->min_im, &win->frac->max_im, y, win->zoom);
}

void    zoom_out(t_win *win, double x, double y)
{
    zoom_axis(&win->frac->min_re, &win->frac->max_re, x, (1 /win->zoom));
    zoom_axis(&win->frac->min_im, &win->frac->max_im, y, (1/ win->zoom));
}
