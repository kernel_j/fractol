/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/11 12:58:48 by jwong             #+#    #+#             */
/*   Updated: 2018/04/26 18:39:28 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "mlx.h"
#include "fractol.h"

int		nova_calc(double z_re, double z_im, int iter)
{
	double	old_zre;
	double	old_zim;
	double	tmp;
	int		i;
	
	i = 0;
	tmp = 1.0;
	while (tmp > 0.000001 && ++i < iter)
	{
		old_zre = z_re;
		old_zim = z_im;
		tmp = (z_re * z_re + z_im * z_im) * (z_re * z_re + z_im * z_im);
		z_re = (2 * z_re * tmp + z_re * z_re - z_im * z_im) / (3.0 * tmp);
		z_im = (2 * z_im * (tmp - old_zre)) / (3.0 * tmp);
		tmp = (z_re - old_zre) * (z_re - old_zre) + (z_im - old_zim) * (z_im - old_zim);
	}
	return (i);
}

void	nova(t_win *win, int x, int y)
{
	double	z_re;
	double	z_im;
	int		i;

	z_im = px_to_mand_y(win, y);
	z_re = px_to_mand_x(win, x);
	i = nova_calc(z_re, z_im, win->frac->max_iter);
	if (win->colour_mode == FALSE)
		put_pixel(win, x, y, colour_smoothing(win, sqrt(z_re * z_re + z_im * z_im), i));
	else
		put_pixel(win, x, y, multi_colour(win, i));
}

void	julia(t_win *win, int x, int y)
{
	double	z_re;
	double	z_im;
	double	z_re2;
	double	z_im2;
	int		i;

	z_im = px_to_mand_y(win, y);
	z_re = px_to_mand_x(win, x);
	i = 0;
	while (i < win->frac->max_iter)
	{
		z_re2 = z_re * z_re;
		z_im2 = z_im * z_im;
		if (z_re2 + z_im2 > 4)
			break ;
		z_im = 2 * z_re * z_im + win->frac->c_im;
		z_re = z_re2 - z_im2 + win->frac->c_re;
		i++;
	}
	if (win->colour_mode == FALSE)
		put_pixel(win, x, y, colour_smoothing(win, sqrt(z_re * z_re + z_im * z_im), i));
	else
		put_pixel(win, x, y, multi_colour(win, i));
}

void	burning_ship(t_win *win, int x, int y)
{
	double	z_re;
	double	z_im;
	double	z_re2;
	double	z_im2;
	int		i;

	z_im = px_to_mand_y(win, y);
	z_re = px_to_mand_x(win, x);
	i = 0;
	while (i < win->frac->max_iter)
	{
		z_re2 = z_re * z_re;
		z_im2 = z_im * z_im;
		if (z_re2 + z_im2 > 4)
			break ;
		z_im = fabs(2 * z_re * z_im + px_to_mand_y(win, y));
		z_re = fabs(z_re2 - z_im2 + px_to_mand_x(win, x));
		i++;
	}
	if (win->colour_mode == FALSE)
		put_pixel(win, x, y, colour_smoothing(win, sqrt(z_re * z_re + z_im * z_im), i));
	else
		put_pixel(win, x, y, multi_colour(win, i));
}