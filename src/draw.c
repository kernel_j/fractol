/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/01 18:16:22 by jwong             #+#    #+#             */
/*   Updated: 2018/05/01 18:16:23 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mlx.h"
#include "fractol.h"

int		draw_fractal(t_win *win)
{
	if (win->fractol == MANDELBROT)
		iterate_coordinates(win, mandelbrot);
	else if (win->fractol == JULIA)
		iterate_coordinates(win, julia);
	else if (win->fractol == BURNING_SHIP)
		iterate_coordinates(win, burning_ship);
	else if (win->fractol == NOVA)
		iterate_coordinates(win, nova);
	return (0);
}

int		redraw(t_win *win)
{
	mlx_destroy_image(win->mlx, win->img_id);
	win->img_id = mlx_new_image(win->mlx, MAX_WIN_X, MAX_WIN_Y);
	win->img = mlx_get_data_addr(win->img_id, &win->bpp, &win->size_line,\
			&win->endian);
	draw_fractal(win);
	mlx_put_image_to_window(win->mlx, win->win, win->img_id, 0, 0);
	return (0);
}
