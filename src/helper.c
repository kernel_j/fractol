/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helper.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/01 18:16:36 by jwong             #+#    #+#             */
/*   Updated: 2018/05/01 18:16:37 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "fractol.h"

double  px_to_mand_x(t_win *win, int x)
{
    return ((x * win->frac->re_factor) + win->frac->min_re);
}

double  px_to_mand_y(t_win *win, int y)
{
    return ((y * win->frac->im_factor) + win->frac->min_im);
}

double  scale_factor_im(t_frac *frac)
{
    double  im_factor;

    im_factor = (frac->max_im - frac->min_im) / MAX_WIN_Y;
    return (im_factor);
}

double  scale_factor_re(t_frac *frac)
{
    double  re_factor;

    re_factor = (frac->max_re - frac->min_re) / MAX_WIN_X;
    return (re_factor);
}
