/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keys.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/01 18:16:43 by jwong             #+#    #+#             */
/*   Updated: 2018/05/01 18:16:45 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void    lock(int *lock)
{
    if (*lock == FALSE)
        *lock = TRUE;
    else
        *lock = FALSE;
}

void    directions(int keycode, t_win *win)
{
    double amount_re;
    double amount_im;

    amount_re = SHIFT * win->frac->re_factor;
    amount_im = SHIFT * win->frac->im_factor;
    if (keycode == KEY_RIGHT)
    {
        win->frac->max_re -= amount_re;
        win->frac->min_re -= amount_re;
    }
    else if (keycode == KEY_LEFT)
    {
        win->frac->max_re += amount_re;
        win->frac->min_re += amount_re;
    }
    else if (keycode == KEY_DOWN)
    {
        win->frac->max_im -= amount_im;
        win->frac->min_im -= amount_im;
    }
    else if (keycode == KEY_UP)
    {
        win->frac->max_im += amount_im;
        win->frac->min_im += amount_im;
    }
}
