/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handler.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/01 14:49:45 by jwong             #+#    #+#             */
/*   Updated: 2018/05/01 14:56:33 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "mlx.h"
#include "fractol.h"

void    clean(t_win *win)
{
    free(win->frac);
    mlx_destroy_image(win->mlx, win->img_id);
    mlx_destroy_window(win->mlx, win->win);
    exit(0);
}

void    modify_iterations(int keycode, t_win *win)
{
    if (keycode == 56)
    {
        if (win->frac->max_iter - 10 > MAX_ITER)
            win->frac->max_iter -= 10;
    }
    else if (keycode == 112)
        win->frac->max_iter += 10;
}

int		key_handler(int keycode, t_win *win)
{
	if (keycode == KEY_ESC)
		clean(win);
    else if (keycode == KEY_SPACE)
        lock(&win->lock);
	else if (keycode == KEY_TAB)
		lock(&win->colour_mode);
	else if (keycode == KEY_PAGEUP)
        increase_hue(win);
	else if (keycode == KEY_PAGEDOWN)
        decrease_hue(win);
    else if (keycode == 59 || keycode == 112)
        modify_iterations(keycode, win);
    else if (keycode == KEY_LEFT || keycode == KEY_RIGHT || keycode == KEY_UP
            || keycode == KEY_DOWN)
        directions(keycode, win);
	return (0);
}

int     mouse_click_handler(int button, int x, int y, t_win *win)
{
    if (x <= 0)
        x = 1;
    else if (y <= 0)
        y = 1;
    if (button == MOUSE_WHEEL_UP || button == MOUSE_CLICK_LEFT)
    {
        win->mouse_x = px_to_mand_x(win, x);
        win->mouse_y = px_to_mand_y(win, y);
        zoom_in(win, win->mouse_x, win->mouse_y);
    }
    else if (button == MOUSE_WHEEL_DOWN || button == MOUSE_CLICK_RIGHT)
    {
        win->mouse_x = px_to_mand_x(win, x);
        win->mouse_y = px_to_mand_y(win, y);
        zoom_out(win, win->mouse_x, win->mouse_y);
    }
    return (0);
}

int     mouse_over_handler(int x, int y, t_win *win)
{
    if (win->fractol != JULIA)
        return (0);
    if (win->lock != FALSE)
        return (0);
    win->frac->c_re = px_to_mand_x(win, x);
    win->frac->c_im = px_to_mand_y(win, y);
    return (0);
}
