/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colour.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/01 18:16:15 by jwong             #+#    #+#             */
/*   Updated: 2018/05/01 18:16:18 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include "fractol.h"

void    increase_hue(t_win *win)
{
    if (win->colour_mode == FALSE)
    {
        if (win->hue + INC_HUE < MAX_HUE)
            win->hue += INC_HUE;
    }
    else
    {
        if (win->freq + INC_FREQ < MAX_FREQ)
            win->freq += INC_FREQ;
    }
}

void    decrease_hue(t_win *win)
{
    if (win->colour_mode == FALSE)
    {
        if (win->hue - INC_HUE > MIN_HUE)
            win->hue -= INC_HUE;
    }
    else
    {
        if (win->freq - INC_FREQ > MIN_FREQ)
            win->freq -= INC_FREQ;
    }
}

int		rgb(int r, int g, int b)
{
	return ((r << 16) | (g << 8) | b);
}

int		multi_colour(t_win *win, int iter)
{
	float	freq;
	int		colour;
	int		r;
	int		g;
	int		b;
	
	if (iter == win->frac->max_iter)
		colour = rgb(0, 0, 0);
	else
	{
		freq = win->freq;
		r = sin(freq * iter + 0) * 127 + 128;
		g = sin(freq * iter + 2) * 127 + 128;
		b = sin(freq * iter + 4) * 127 + 128;
		colour = rgb(r, g, b);
	}
	return (colour);
}

int		colour_smoothing(t_win *win, double z, int iter)
{
	int		colour;
	double	brightness;

	if (iter == win->frac->max_iter)
		colour = rgb(0, 0, 0);
	else
	{
		brightness = 256.0 * log(1.75 + iter - log(log(z))) / log(win->frac->max_iter);
		colour = rgb(brightness, brightness, win->hue);
	}
	return (colour);
}
