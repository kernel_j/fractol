# README #
Fract'ol is a program that allows your to visualize and explore 4 different
types of fractals.

### Install ###
Compile the program using the rules of the Makefile on the command line
```
make
```

To re-compile the project
```
make re
```

### Usage ###
Fract'ol takes one arguement; the type of fractal to be visualize.
Types of fractals:

* Mandelbot
* Julia
* Burning_ship
* Nova

To run Fract'ol
```
./fractol mandelbrot
```

### Controls ###
Fract'ol has both directional controls as well as parameter controls.

#### Displacement ####
Move the fractal via the arrow keys

#### Zoom ####

* Zoom in : __Left Mouse Click / Scroll Up__
* Zoom out : __Right Mouse Click / Scroll Down__

#### Pause Image ####
To pause and un-pause the fractal Julia : __Space__

#### Iterations ####
Increasing and decreasing the number of iterations allows you to see more
details.

* To increase the number of iterations : __Plus__ on number pad
* To decrease the number of iterations : __Minus__ on number pad

#### Colour ####
To switch between colour shading and colour histogram : __Tab__

In colour shading mode

* Increse hue : __Page Up__ key
* Decrease hue : __Page Down__ key

In colour histogram mode

* Increase frequency : __Page Up__ key
* Decrease frequency : __Page Down__ key

### Uninstall ###
To remove all object files
```
make clean
```

To remove all object files and the executable
```
make fclean
```
