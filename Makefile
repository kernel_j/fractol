# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jwong <marvin@42.fr>                       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/04/11 12:56:24 by jwong             #+#    #+#              #
#    Updated: 2018/05/02 16:49:27 by jwong            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME	= fractol

SRC		= colour.c		\
		  draw.c		\
		  error.c		\
		  fractol.c		\
		  handler.c		\
		  helper.c 		\
		  init.c		\
		  keys.c 		\
		  main.c		\
		  mandelbrot.c	\
		  util.c		\
		  zoom.c 		\

CC		= gcc
DIR		= src/
SRCS	= $(addprefix $(DIR), $(SRC))
OBJ		= $(SRCS:.c=.o)

PLATFORM	:= $(shell uname)

ifeq	($(PLATFORM), Linux)
CFLAGS	= -Wall -Werror -Wextra -I libft/includes -I minilibx -I inc/
else ifeq	($(PLATFORM), Darwin)
CFLAGS	= -Wall -Werror -Wextra -I libft/includes -I minilibx_macos -I inc/
endif

all: $(NAME)

ifeq	($(PLATFORM), Linux)
$(NAME): $(OBJ)
		make -C libft/
		$(CC) $(CFLAGS) $(OBJ) -o $(NAME) -L libft/ -L minilibx/ -lmlx -lXext -lX11 -lft -lm
		
%.o: %.c
		$(CC) $(CFLAGS) $< -c -o $@

clean:
		make -C libft/ clean
		rm -f $(OBJ)

fclean: clean
		make -C libft/ fclean
		rm -f $(NAME)

re: fclean all

else ifeq	($(PLATFORM), Darwin)
$(NAME): $(OBJ)
		make -C libft/
		$(CC) $(CFLAGS) $(OBJ) -o $(NAME) -L libft/ -L minilibx_macos/ -framework OpenGL -framework AppKit -lmlx -lft -lm
		
%.o: %.c
		$(CC) $(CFLAGS) $< -c -o $@

clean:
		make -C minilibx_macos/ clean
		make -C libft/ clean
		rm -f $(OBJ)

fclean: clean
		make -C libft/ fclean
		rm -f $(NAME)

re: fclean all

endif
