/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/11 12:57:14 by jwong             #+#    #+#             */
/*   Updated: 2018/05/02 16:52:21 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifdef __APPLE__
# define KEY_ESC 53
# define KEY_SPACE 49
# define KEY_TAB 48
# define KEY_PAGEUP 116
# define KEY_PAGEDOWN 121
# define KEY_LEFT 123
# define KEY_RIGHT 124
# define KEY_UP 126
# define KEY_DOWN 125
# define MOUSE_WHEEL_UP 7
# define MOUSE_WHEEL_DOWN 4
# define MOUSE_CLICK_LEFT 1
# define MOUSE_CLICK_RIGHT 2
#elif __unix__
# define KEY_ESC 65307
# define KEY_SPACE 32
# define KEY_TAB 65289
# define KEY_PAGEUP 65365
# define KEY_PAGEDOWN 65366
# define KEY_LEFT 65361
# define KEY_RIGHT 65363
# define KEY_UP 65362
# define KEY_DOWN 65364
# define MOUSE_WHEEL_UP 4
# define MOUSE_WHEEL_DOWN 5
# define MOUSE_CLICK_LEFT 1
# define MOUSE_CLICK_RIGHT 3
#endif

#ifndef FRACTOL_H
# define FRACTOL_H

# define FALSE -1
# define TRUE 1
# define MAX_WIN_X 1000
# define MAX_WIN_Y 1000
# define MIN_RE -2.0
# define MAX_RE 2.0
# define MIN_IM -2.0
# define MAX_IM 2.0
# define MAX_ITER 50
# define SHIFT 20
# define ZOOM 0.98
# define FREQUENCY 0.3
# define INC_FREQ 0.1
# define MAX_FREQ 5.0
# define MIN_FREQ 0.1
# define INC_HUE 5
# define MAX_HUE 256
# define MIN_HUE 1

enum	e_error
{
	ARG_ERR,
	MALLOC_ERR
};

enum	e_fractol
{
	MANDELBROT,
	JULIA,
    BURNING_SHIP,
	NOVA
};

typedef struct  s_frac
{
    double      max_re;
    double      min_re;
    double      max_im;
    double      min_im;
    double      c_re;
    double      c_im;
    double      im_factor;
    double      re_factor;
    double      max_iter;

}               t_frac;

typedef struct	s_win
{
	void		*mlx;
	void		*win;
	void		*img_id;
	char		*img;
	int			bpp;
	int			size_line;
	int			endian;
	int			fractol;
    int         lock;
	int			colour_mode;
    t_frac      *frac;
    double      zoom;
    double      mouse_x;
    double      mouse_y;
    float       freq;
    int         hue;
}				t_win;

/*
** colour.c
*/
int     colour_smoothing(t_win *win, double z, int iter);
int     multi_colour(t_win *win, int iter);
void    decrease_hue(t_win *win);
void    increase_hue(t_win *win);

/*
** draw.c
*/
int     draw_fractal(t_win *win);
int     redraw(t_win *win);

/*
** error.c
*/
int		error_handler(int err);

/*
** fractol.c
*/
void    nova(t_win *win, int x, int y);
void    julia(t_win *win, int x, int y);
void    burning_ship(t_win *win, int x, int y);

/*
** handler.c
*/
int     key_handler(int keycode, t_win *win);
int     mouse_click_handler(int button, int x, int y, t_win *win);
int     mouse_over_handler(int x, int y, t_win *win);

/*
** helper.c
*/
double  px_to_mand_x(t_win *win, int x);
double  px_to_mand_y(t_win *win, int y);
double  scale_factor_im(t_frac *frac);
double  scale_factor_re(t_frac *frac);

/*
** init.c
*/
int     set_fractal(t_win *win, char *param);
void    init_win(t_win *win);
t_frac  *init_frac(void);

/*
** keys.c
*/
void    lock(int *lock);
void    directions(int keycode, t_win *win);

/*
** mandelbrot.c
*/
void    mandelbrot(t_win *win, int x, int y);

/*
** util.c
*/
void	put_pixel(t_win *win, int x, int y, int rgb);
char    *string_to_lower(const char *src, char *dest, int len);
int     iterate_coordinates(t_win *win, void (*fractal)(t_win *, int, int));

/*
** zoom.c
*/
void    zoom_in(t_win *win, double x, double y);
void    zoom_out(t_win *win, double x, double y);

#endif
